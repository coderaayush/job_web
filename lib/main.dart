import 'package:job_web/app.dart';
import 'package:job_web/bootstrap.dart';

void main(List<String> args) {
  bootstrap(App.new);
}
