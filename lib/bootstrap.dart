import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:job_web/app.dart';

/// Bootstrap the application.
Future<void> bootstrap(FutureOr<Widget> Function() builder) async {
  runApp(const App());
}
