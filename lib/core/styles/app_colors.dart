import 'package:flutter/material.dart';

class AppColors {
  const AppColors._();

  static const Color primaryColor = Color(0xff319795);
  static const Color redColor = Color(0xFFFF5959);
  static const LinearGradient primaryGradient = LinearGradient(
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
    colors: [
      Color(0xff3093a0),
      Color(0xff3385c8),
    ],
  );

  static const Color transparentColor = Colors.transparent;

  static const Color blackColor = Color(0xFF000000);
  static const Color whiteColor = Color(0xFFFFFFFF);

  static const Color midGreyColor = Color(0xFF989898);
  static const Color grey10 = Color(0xffe9fbfe);
  static const Color greyColor = Color(0xFFDDDDDD);
  static const Color lightGreyColor = Color(0xFFD6D6D6);
  static const Color grey400 = Color(0xff98A5AF);
  static const Color disabled = Color(0xffF1F1F1);
  static const Color darkGreyColor = Color(0xFF989898);

  static const Color lightBlack = Color(0xff2d3748);
  static const Color softWhite = Color(0xffcbd5e0);
  static const Color softGreen = Color(0xff82e5d9);
  static const Color darkBlackColor = Color(0xff4a5468);
  static const Color softGreenColor = Color(0xff4a5468);
  static const Color lightSoftGreenColor = Color(0xff718096);
  static const Color lightWhite = Color(0xffe9f7fe);
}
