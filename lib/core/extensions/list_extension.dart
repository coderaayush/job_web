extension Listx<T> on List<T> {
  List<T> doReverse(bool isTrue) {
    if (!isTrue) {
      return this;
    } else {
      return reversed.toList();
    }
  }
}
