// final isMobile = constraints.maxWidth < 600;
// final isTablet = constraints.maxWidth < 1200;
// final isDesktop = constraints.maxWidth >= 1200;

import 'package:flutter/material.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/styles/app_styles.dart';
import 'package:responsive_framework/responsive_framework.dart';

extension ContextX on BuildContext {
  double get width => MediaQuery.of(this).size.width;
  double get height => MediaQuery.of(this).size.height;
  double get textScaleFactor => MediaQuery.of(this).textScaleFactor;

  MediaQueryData get mediaQuery => MediaQuery.of(this);

  bool get isMobile => ResponsiveBreakpoints.of(this).isMobile;

  bool get isTablet => ResponsiveBreakpoints.of(this).isTablet;

  bool get isDesktop => ResponsiveBreakpoints.of(this).isDesktop;
}

extension TextStyleX on TextStyle {
  //colors
  TextStyle get primary => copyWith(color: AppColors.primaryColor);

  TextStyle get red => copyWith(color: AppColors.redColor);

  TextStyle get black => copyWith(color: AppColors.blackColor);

  TextStyle get white => copyWith(color: AppColors.whiteColor);
  TextStyle get softGreen => copyWith(color: AppColors.softGreenColor);
  TextStyle get lightSoftGreen => copyWith(color: AppColors.lightSoftGreenColor);

  TextStyle get midGrey => copyWith(color: AppColors.midGreyColor);

  TextStyle get grey => copyWith(color: AppColors.greyColor);

  TextStyle get grey400 => copyWith(color: AppColors.grey400);

  TextStyle get darkGrey => copyWith(color: AppColors.darkGreyColor);
  TextStyle get darkBlack => copyWith(color: AppColors.darkBlackColor);

  TextStyle lineHeight(double value) => copyWith(height: value / fontSize!);

  TextStyle spacing(double value) => copyWith(letterSpacing: calculateSpacing(value));
}

extension WidgetX on Widget {
  Padding px(double padding) => Padding(
        padding: EdgeInsets.symmetric(horizontal: padding),
        child: this,
      );

  Padding py(double padding) => Padding(
        padding: EdgeInsets.symmetric(vertical: padding),
        child: this,
      );

  Scaffold get scaffold => Scaffold(body: this);

  Padding pOnly({
    double top = 0,
    double right = 0,
    double bottom = 0,
    double left = 0,
  }) =>
      Padding(
        padding: EdgeInsets.only(
          top: top,
          right: right,
          bottom: bottom,
          left: left,
        ),
        child: this,
      );

  Padding pad(double value) => Padding(
        padding: EdgeInsets.all(value),
        child: this,
      );
}
