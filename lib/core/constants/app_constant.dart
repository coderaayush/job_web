class Tabs {
  const Tabs._();

  static const String home = 'Arbeitnehmer';
  static const String search = 'Arbeitgeber';
  static const String profile = 'Temporarburo';
}

final tabs = [
  const TabEntity(title: Tabs.home, id: 0),
  const TabEntity(title: Tabs.search, id: 1),
  const TabEntity(title: Tabs.profile, id: 2),
];

class TabEntity {
  const TabEntity({
    required this.title,
    required this.id,
    this.isSelected = false,
  });

  final String title;
  final int id;
  final bool isSelected;
}
