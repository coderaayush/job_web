import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/styles/app_styles.dart';

class GradientButton extends StatelessWidget {
  /// [title] argument is required
  const GradientButton({
    Key? key,
    required this.title,
    this.onPressed,
    this.titleStyle,
    this.gradient = AppColors.primaryGradient,
    this.shape,
    this.width = 140,
    this.height = 40,
    this.loading = false,
    this.isDisabled = true,
    this.icon,
  }) : super(key: key);

  final String title;
  final Widget? icon;

  final VoidCallback? onPressed;

  /// [titleStyle] is used to style the button text
  final TextStyle? titleStyle;

  /// [gradient] for enabled state of button
  final Gradient gradient;

  /// [shape] is used to apply border radius on button,
  final ShapeBorder? shape;

  /// [width] button width, defaults is 140
  final double width;

  /// [height] button height, defaults is 44
  final double height;

  /// [loading] is used to display circular progress indicator on loading event, default is false
  final bool loading;

  /// [isDisabled] is used to disable to button, default is true
  final bool isDisabled;

  ShapeBorder get _shape => shape ?? RoundedRectangleBorder(borderRadius: BorderRadius.circular(4));

  BoxConstraints get _constraints => BoxConstraints.tightFor(width: width, height: height);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      clipBehavior: Clip.antiAlias,
      shape: _shape,
      child: InkWell(
        splashColor: AppColors.whiteColor.withOpacity(0.4),
        onTap: isDisabled || loading ? null : onPressed,
        child: ConstrainedBox(
          constraints: _constraints,
          child: Ink(
            decoration: ShapeDecoration(
              color: isDisabled ? AppColors.greyColor : null,
              gradient: isDisabled ? null : gradient,
              shape: _shape,
            ),
            child: loading
                ? const Center(
                    child: SizedBox(
                      width: 24,
                      height: 24,
                      child: CircularProgressIndicator(
                        color: AppColors.whiteColor,
                        strokeWidth: 2,
                      ),
                    ),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Text(title, style: AppStyles.text12PxMedium.white.copyWith(fontSize: 18)),
                      ),
                      if (icon != null) ...[
                        12.horizontalSpace,
                        icon!,
                      ],
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
