import 'package:flutter/material.dart';

/// Clip widget in wave shape shape
class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Offset firstEndPoint = Offset(size.width * .5, 20);
    Offset firstControlPoint = Offset(size.width * .25, 30);
    Offset secondEndPoint = Offset(size.width, 50);
    Offset secondControlPoint = Offset(size.width * .75, 10);

    final path = Path()
      ..quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPoint.dx, firstEndPoint.dy)
      ..quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy)
      ..lineTo(size.width, size.height)
      ..lineTo(0.0, size.height)
      ..close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
