import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:job_web/screens/screens.dart';

part 'app_router.g.dart';

final appRouter = GoRouter(routes: $appRoutes);

@TypedGoRoute<HomeRoute>(path: '/')
class HomeRoute extends GoRouteData {
  const HomeRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const HomeScreen();
}
