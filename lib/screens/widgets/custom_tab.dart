import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/styles/app_styles.dart';

class CustomTab extends StatelessWidget {
  const CustomTab({
    super.key,
    required this.index,
    required this.title,
    required this.onChange,
    this.isActive = false,
  });

  final int index;
  final bool isActive;
  final String title;
  final ValueChanged<int> onChange;

  Radius get radius => const Radius.circular(12);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: renderRadius(index),
        border: Border.all(color: AppColors.softWhite),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        onTap: () => onChange.call(index),
        child: AnimatedContainer(
          decoration: BoxDecoration(
            color: isActive ? AppColors.softGreen : AppColors.transparentColor,
            borderRadius: renderRadius(index),
          ),
          duration: const Duration(milliseconds: 300),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: isActive ? AppStyles.text14PxSemiBold.white.copyWith(fontSize: 14) : AppStyles.text14PxSemiBold.midGrey.copyWith(fontSize: 14),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ).px(20.w).pOnly(top: 5, bottom: 5),
            ],
          ).px(10).py(10),
        ),
      ),
    );
  }

  BorderRadiusGeometry renderRadius(int index) {
    if (index == 0) {
      return BorderRadius.only(
        topLeft: radius,
        bottomLeft: radius,
      );
    } else if (index == 2) {
      return BorderRadius.only(
        topRight: radius,
        bottomRight: radius,
      );
    } else {
      return BorderRadius.zero;
    }
  }
}
