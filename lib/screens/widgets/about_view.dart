import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/screens/widgets/widgets.dart';

class AboutView extends StatelessWidget {
  AboutView({Key? key}) : super(key: key);

  final ValueNotifier<int> _notifier = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        20.verticalSpace,
        ValueListenableBuilder(
          valueListenable: _notifier,
          builder: (context, value, _) {
            return CustomTabs(
              currentIndex: _notifier.value,
              onChanged: (value) => _notifier.value = value,
            );
          },
        ),
      ],
    );
  }
}
