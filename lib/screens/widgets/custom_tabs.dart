import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/constants/app_constant.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/screens/widgets/widgets.dart';

class CustomTabs extends StatelessWidget {
  const CustomTabs({
    Key? key,
    required this.onChanged,
    this.currentIndex = 0,
  }) : super(key: key);
  final ValueChanged<int> onChanged;
  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: tabs
              .map(
                (e) => CustomTab(
                  index: e.id,
                  onChange: (value) => onChanged.call(value),
                  isActive: currentIndex == e.id,
                  title: e.title,
                ),
              )
              .toList(),
        ).pOnly(right: 20.w, left: 20.w),
      ),
    );
  }
}
