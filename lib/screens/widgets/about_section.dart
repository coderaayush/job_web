import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/styles/app_styles.dart';
import 'package:job_web/gen/assets.gen.dart';
import 'package:job_web/screens/widgets/widgets.dart';

class AboutSection extends StatelessWidget {
  const AboutSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        35.verticalSpace,
        Text('Drei einfache Schritte', style: AppStyles.text14PxMedium.darkBlack),
        2.verticalSpace,
        Text('zu deinem neuen Job', style: AppStyles.text14PxMedium.darkBlack),
        20.verticalSpace,
        AboutTile(
          count: 1,
          title: 'Erstellen dein Lebenslauf',
          image: Assets.illustrations.illustration1.path,
        ),
        20.verticalSpace,
        AboutTile(
          count: 2,
          title: 'Erstellen dein Lebenslauf',
          image: Assets.illustrations.automation.path,
          reverse: true,
          filled: true,
          titlePosition: TitlePos.bottom,
        ),
        20.verticalSpace,
        Builder(builder: (context) {
          final title = StringBuffer();
          context.isMobile ? title.write('Mit nur einem Klick') : title.write('Mit nu\neinem Klick bewerben');

          return AboutTile(
            count: 3,
            title: title.toString(),
            image: Assets.illustrations.illustration1.path,
            titlePosition: TitlePos.bottom,
          );
        }),
        20.verticalSpace,
      ],
    );
  }
}
