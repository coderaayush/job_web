import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/painters/wavy_clipper.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/styles/app_styles.dart';

class AboutTile extends StatelessWidget {
  final int count;
  final String title;
  final String image;
  final bool filled;
  final bool reverse;
  final TitlePos titlePosition;
  const AboutTile({
    Key? key,
    required this.count,
    required this.title,
    required this.image,
    this.reverse = false,
    this.filled = false,
    this.titlePosition = TitlePos.top,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WaveClipper(),
      child: Container(
        padding: EdgeInsets.all(14.sp),
        color: filled ? AppColors.lightWhite : null,
        child: Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: context.isMobile ? context.width : 1150,
            ),
            child: Column(
              children: getBody(context),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> getBody(BuildContext context) {
    return [
      ...[
        if (context.isMobile && titlePosition == TitlePos.top)
          Image.asset(
            image,
            width: 250.w,
            fit: BoxFit.cover,
          ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (reverse && !context.isMobile)
              Expanded(
                child: Image.asset(
                  image,
                  width: 300.w,
                  height: 300.h,
                  fit: BoxFit.contain,
                ),
              ),
            Text('${count.toString()}.', style: AppStyles.text26PxBold.softGreen),
            Text(title, style: AppStyles.text14PxMedium.lightSoftGreen).pOnly(bottom: !context.isMobile ? 10 : 3.w),
            20.horizontalSpace,
            if (!context.isMobile && !reverse)
              Expanded(
                child: Image.asset(
                  image,
                  width: 300.w,
                  height: 300.h,
                  fit: BoxFit.contain,
                ),
              ),
          ],
        ),
        if (context.isMobile && titlePosition == TitlePos.bottom)
          Image.asset(
            image,
            width: 300.w,
            height: 300.h,
            fit: BoxFit.contain,
          )
      ]
    ];
  }
}

enum TitlePos { top, bottom }
