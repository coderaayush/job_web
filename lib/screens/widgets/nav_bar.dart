import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/styles/app_styles.dart';

class NavBar extends StatelessWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 8,
          decoration: const BoxDecoration(gradient: AppColors.primaryGradient),
        ),
        Container(
          width: double.infinity,
          height: 60,
          decoration: BoxDecoration(
            color: AppColors.whiteColor,
            borderRadius: const BorderRadius.vertical(bottom: Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: AppColors.blackColor.withOpacity(0.3),
                blurRadius: 8,
                offset: const Offset(0, 2),
              ),
            ],
          ),
          child: Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {},
              child: Text('Login', style: AppStyles.text10PxMedium.primary.copyWith(fontSize: 18)),
            ),
          ).pOnly(right: 20.w),
        ),
      ],
    );
  }
}
