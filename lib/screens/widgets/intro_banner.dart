import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/painters/slanted_painter.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/styles/app_styles.dart';
import 'package:job_web/core/widgets/custom_gradient_button.dart';
import 'package:job_web/gen/assets.gen.dart';

class IntroBanner extends StatelessWidget {
  const IntroBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: SlantedPainter(),
      child: LayoutBuilder(builder: (context, constraints) {
        return Container(
            height: (context.height * .7),
            width: double.infinity,
            decoration: const BoxDecoration(color: AppColors.grey10),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      10.verticalSpace,
                      Align(
                        alignment: context.isMobile ? Alignment.center : Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: context.isMobile ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Deine Job website',
                              style: AppStyles.text24PxBold.black,
                              textAlign: context.isMobile ? TextAlign.center : TextAlign.start,
                            ),
                            Text(
                              'website',
                              style: AppStyles.text24PxBold.black,
                              textAlign: context.isMobile ? TextAlign.center : TextAlign.start,
                            ),
                          ],
                        ),
                      ),
                      10.verticalSpace,
                      if (!context.isMobile)
                        GradientButton(
                          width: 400,
                          title: 'Kostenlos Registrierene',
                          isDisabled: false,
                          onPressed: () {},
                        ),
                      if (context.isMobile) ...[
                        20.verticalSpace,
                        Assets.illustrations.jobIllustration.svg(height: 250.sp, width: 250.sp),
                      ]
                    ],
                  ),
                  if (!context.isMobile)
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Assets.illustrations.jobIllustration.svg(height: 300.sp, width: 300.sp),
                      ).pOnly(left: 10.w),
                    ),
                ],
              ).px(20.w),
            ));
      }),
    );
  }
}
