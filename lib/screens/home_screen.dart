import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:job_web/core/extensions/extensions.dart';
import 'package:job_web/core/styles/app_colors.dart';
import 'package:job_web/core/widgets/custom_gradient_button.dart';
import 'package:job_web/screens/widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          bottomNavigationBar: context.isMobile
              ? BottomAppBar(
                  elevation: 16,
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: GradientButton(
                      title: 'Kostenlos Registrierene',
                      isDisabled: false,
                      onPressed: () {},
                    ),
                  ),
                )
              : null,
          backgroundColor: AppColors.whiteColor,
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    // Fixed height for desktop
                    60.verticalSpace,
                    const IntroBanner(),
                    AboutView(),
                    const AboutSection(),
                  ],
                ),
              ),
              Positioned(
                top: 0,
                width: context.width,
                child: const NavBar(),
              ),
            ],
          ),
        );
      },
    );
  }
}
